int find_fdf(char *str)
{
    int i = 0;

    if (str[i] && str[i] == '.')
    {
        i++;
        if (str[i] && str[i] == 'f')
        {
            i++;
            if (str[i] && str[i] == 'd')
            {
                i++;
                if(str[i] && str[i] == 'f')
                {
                    i++;
                    if (!str[i])
                        return (1);
                }
            }
        }
    }
    exit(0);
}

void    is_fdf(char *str)
{
    int i;

    i = 0;
    while(str[i])
    {
        if (find_fdf(str + i) == 1)
            return (1);
        i++;
    }
    ft_putstr_fd("Arg not .fdf")
    return (0);
}

void    check_args(int argc, char **argv)
{
    int i;
    
    if (argc != 2)
    {
        ft_putstr_fd("Wrong nb args\n", 2);
        exit(-1);
    }
    is_fdf(char *argv[1]);
}

int main(int argc, char **argv)
{
    int fd;

    check_args(argc, argv);
    fd = open(argv[1], O_RDONLY);
    if (fd == -1)
    {
        ft_putstr_fd("could not open fiel \n", 2);
        exit(-1);
    }
    save_map(fd);
    close(fd);
    
    return (0);
}