/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   condition.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvittoz <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/05/19 22:56:25 by jvittoz           #+#    #+#             */
/*   Updated: 2024/06/18 15:40:46 by jvittoz          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "../include/pipex.h"

void	ft_free_split(char **split)
{
	int	i;

	i = 0;
	while (split[i])
		free(split[i++]);
	free(split);
}

int	is_substr(char *from, char *to)
{
	int	j;

	j = 0;
	while (from[j] && to[j] && from[j] == to[j])
		j++;
	if (from[j] == 0)
		return (1);
	return (-1);
}

int	find_in_env(char *str, char **env)
{
	int i;

	if (env == 0 || str == 0)
		return (-1);
	i = 0;
	while (env[i])
	{
		if (is_substr(str, env[i]) == 1)
			return (i);
		i++;
	}
	return (-1);
}

char	*get_command(char *cmd, char **env)
{
	int		fie;
	int		i;
	char	**path;
	char	*pre_path;
	char	*res;

	fie = find_in_env("PATH=", env);
	if(fie == -1)
		return (NULL);
	path = ft_split(env[fie] + 5, ':');
	if (path == 0)
		return (NULL);
	i = 0;
	while (path[i])
	{
		pre_path = ft_strjoin(path[i], "/");
		if (pre_path == 0)
			break;
		res = ft_strjoin(pre_path, cmd);
		free(pre_path);
		if (res == 0)
			break;
		if (access(res, X_OK) == 0)
		{
			ft_free_split(path);
			ft_printf("%s\n",res);
			return (res);
		}
		free(res);
		i++;
	}
	ft_free_split(path);
	return (NULL);
}

int	ch1(s_data data, int file, char **argv, char **env)
{
	char	*cmd;
	char	**re_args;

	dup2(data.p_fd[1], 1);
	close(data.p_fd[0]);
	dup2(file, 0);
	re_args = ft_split(argv[2], ' ');
	if (re_args == 0)
		return(-1);
	cmd = get_command(re_args[0], env);
	if (cmd == 0)
	{
		ft_free_split(re_args);
		return (-1);
	}
	execve(cmd, re_args, env);
	free(re_args);
	free(cmd);
	return (1);
}

int	ch2(s_data data, int file, char **argv, char **env)
{
	char	*cmd;
	char	**re_args;

	dup2(data.p_fd[0], 0);
	close(data.p_fd[1]);
	dup2(file, 1);
	re_args = ft_split(argv[3], ' ');
	if (re_args == 0)
		return (-1);
	cmd = get_command(re_args[0], env);
	if (cmd == 0)
	{
		ft_free_split(re_args);
		return (-1);
	}
	execve(cmd, re_args, env);
	free(re_args);
	free(cmd);
	return (1);
}

int main(int argc, char **argv, char **env)
{
	int 	file1;
	int		file2;
	s_data	data;
	printf("yes\n");

	if (argc != 5)
		return (-1);
	file1 = open(argv[1], O_RDONLY);
	if (file1 == -1)
	{
		perror("file1\n");
		return (-1);
	}
	file2 = open(argv[4], O_CREAT | O_RDWR | O_TRUNC, 0777);
	if (file2 == -1)
	{
		perror("file2\n");
		return (-1);
	}
	printf("yes\n");
	if (pipe(data.p_fd) == -1)
		return(-1);
	data.pid1 = fork();
	if (data.pid1 == 0)
		ch1(data, file1, argv, env);
	data.pid2 = fork();
	if (data.pid2 == 0)
		ch2(data, file2, argv, env);
	close(data.p_fd[0]);
	close(data.p_fd[1]);
	waitpid(data.pid1, NULL, 0);
	waitpid(data.pid2, NULL, 0);
	close(file1);
	close(file2);
	return (0);
}
