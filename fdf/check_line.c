#include "fdf.h"

int nb_points(char **tmp)
{
    int i;
    int res;

    res = 0;
    i = 0;
    while (tmp[i])
    {
        if (ft_strlen(tmp[i]) > 0)
        {
            if (ft_isnumber[temp[i]])
                res++;
        }
        i++;
    }
    return (res)
}

short_check(t_data *data, int i)
{
    int     x;
    char    **tmp

    tmp = ft_split(data->map.line, ' ');
    if (tmp == 0)
        return (0);
    x = 0;
    if (i == 0)
        data->map.size_x = nb_points(tmp);
    if (data->map.size_x != nb_points(tmp))
        map_check(tmp, data, i);
    data->map.points[i] = malloc();
    if (data->map.points[i] == 0)
        clean_points(data, i);
    data->map.color[i] = malloc();
    if (data->map.color[i] == 0)
        clean_color(data, i);
    set_point(data, tmp, i);
    ft_free_split(tmp);
    return(1); 
}

int check_line(t_data *data)
{
    int i;

    i = 0;
    data->map.point = malloc(sizeof(t_vector *) * data->map.size_y);
    if (data->map.point == 0)
        return (0);
    data->map.color = malloc(sizeof(t_color *) * data->map.size_y);
    if (data->map.color == 0)
        return (0);
    while (i < data->map.size_y)
    {
        if (short_check(data, i) == 0)
            clear_check(data);
        i++;
    }
    return (1);
}