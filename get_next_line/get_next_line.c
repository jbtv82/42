/* ************************************************************************** */
/*		   																	  */
/*														:::	  ::::::::        */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*													+:+ +:+		 +:+	      */
/*   By: jvittoz <marvin@42.fr>					 +#+  +:+	   +#+		      */
/*												+#+#+#+#+#+   +#+		      */
/*   Created: 2024/01/06 09:00:13 by jvittoz		   #+#	#+#			      */
/*   Updated: 2024/03/16 13:50:35 by jvittoz          ###   ########.fr       */
/*																			  */
/* ************************************************************************** */

#include "get_next_line.h"

char	*ft_in_buffer(char *buffer, int fd)
{
	int		byte;
	char	*byte_buffer;

	byte = 1;
	byte_buffer = ft_calloc(BUFFER_SIZE + 1, sizeof(char));
	if (byte_buffer == 0)
		return (0);
	while (byte > 0)
	{
		byte = read(fd, byte_buffer, BUFFER_SIZE);
		if (byte == -1)
		{
			free(byte_buffer);
			return (0);
		}
		byte_buffer[byte] = 0;
		buffer = ft_join(buffer, byte_buffer);
		if (byte_buffer && byte_buffer[0] != 0
			&& ft_search(byte_buffer, '\n') == 1)
			break ;
	}
	free(byte_buffer);
	return (buffer);
}

char	*ft_first_line(char *buffer)
{
	char	*res;
	int		i;

	if (buffer[0] == 0)
		return (0);
	i = 0;
	while (buffer[i] && buffer[i] != '\n')
		i++;
	res = ft_calloc (i + 2, sizeof(char));
	if (res == 0)
		return (0);
	i = 0;
	while (buffer[i] && buffer[i] != '\n')
	{
		res[i] = buffer[i];
		i++;
	}
	if (buffer[i] && buffer[i] == '\n')
		res[i++] = '\n';
	return (res);
}

char	*ft_out_buffer(char *buffer)
{
	char	*res;
	int		i;
	int		j;

	i = 0;
	j = 0;
	while (buffer[i] && buffer[i] != '\n')
		i++;
	if (buffer[i] == 0)
	{
		free(buffer);
		return (0);
	}
	res = ft_calloc(ft_strlen(buffer) - i + 1, sizeof(char));
	i++;
	while (buffer[i])
		res[j++] = buffer[i++];
	free(buffer);
	return (res);
}

char	*get_next_line(int fd)
{
	static char	*buffer;
	char		*res;

	if (fd < 0 || BUFFER_SIZE <= 0 || read(fd, 0, 0) < 0)
		return (0);
	if (buffer == 0)
		buffer = ft_calloc(1, 1);
	if (!buffer)
		return (0);
	buffer = ft_in_buffer(buffer, fd);
	res = ft_first_line(buffer);
	if (!res && buffer)
	buffer = ft_out_buffer(buffer);
	return (res);
}
