#ifndef FDF_H
#define FDF_H

typedef struct s_vector
{
    int x;
    int y;
    int z;
}t_vector;

typedef struct s_data
{

}t_data;

typedef struct s_color
{
    int bleu;
    int green;
    int red;
}t_color;

typedef struct s_map
{
    char        **line;
    int         x_max;
    int         y_max;
    int         z_min;
    int         z_max;
    float       z_type;
    t_color     *color;
    t_vector   *vector
}t_map;

typedef struct s_data
{
    int     size_x;
    int     size_y;
    t_map   map
}

#endif